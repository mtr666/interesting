/**
 * 幸运大转盘
 * @param {Object} a	大转盘数据
 * @param {Object} b	大转盘第一个选项的权重越大几率越大
 * @param {Object} selected	选择器,不填默认body
 * @date 2020年1月22日
 * @author mtr
 */
var LuckyTurning = function(a, b,selected){
	
	//保存所有奖项
	var awards = a || [
		{name: "谢谢参与",color: "#FFF4D6",imgPath: "img/0.png"},
		{name: "1个币",color: "#FFFFFF",imgPath: "img/10.png"},
		{name: "2个币",color: "#FFF4D6",imgPath: "img/10.png"},
	];
	
	var heiheihei = b || 10;
	
	var	outsideRadius = 192;//大转盘外圆的半径
	var	textRadius = 155;//大转盘奖品位置距离圆心的距离
	var	insideRadius = 68;//大转盘内圆的半径
	var	startAngle = 0;//开始角度
	
	//转盘状态 false:停止;ture:旋转
	var bRotate = false; 
	
	//保存中奖的信息
	var winning = null;
	
	/**
	 * 绘制转盘
	 */
	function drawRouletteWheel() {
		var canvas = document.getElementById("mtrWheelCanvas"); 
		if (canvas.getContext) {
			//奖品个数
			var awardSize = awards.length;
			//根据奖品个数计算圆周角度
			var arc = Math.PI / (awardSize/2);
			var ctx = canvas.getContext("2d");
			//在给定矩形内清空一个矩形
			ctx.clearRect(0,0,422,422);
			//strokeStyle 属性设置或返回用于笔触的颜色、渐变或模式
			ctx.strokeStyle = "#FFBE04";
			//font 属性设置或返回画布上文本内容的当前字体属性
			ctx.font = '16px Microsoft YaHei';
			var oneAwardAngle = 360 / awardSize;
			for(var i = 0; i < awardSize; i++){
				var award = awards[i];
				//设置中这个奖时转盘可以停的位置
				award.startAngle = oneAwardAngle * i;
				award.endAngle = oneAwardAngle * (i + 1);
				var angle = startAngle + i * arc;
				ctx.fillStyle = award.color;
				ctx.beginPath();
				//arc(x,y,r,起始角,结束角,绘制方向) 方法创建弧/曲线（用于创建圆或部分圆）
				ctx.arc(211, 211, outsideRadius, angle, angle + arc, false);
				ctx.arc(211, 211, insideRadius, angle + arc, angle, true);
				ctx.stroke();  
				ctx.fill();
				//锁画布(为了保存之前的画布状态)
				ctx.save();
				
				//----绘制奖品开始----
				ctx.fillStyle = "#E5302F";
				var text = award.name;
				var line_height = 17;
				//translate方法重新映射画布上的 (0,0) 位置
				ctx.translate(211 + Math.cos(angle + arc / 2) * textRadius, 211 + Math.sin(angle + arc / 2) * textRadius);
				
				//rotate方法旋转当前的绘图
				ctx.rotate(angle + arc / 2 + Math.PI / 2);
				//绘制文字
				if(text.length>6){//奖品名称长度超过一定范围
					text = text.substring(0,6)+"||"+text.substring(6);
					var texts = text.split("||");
					for(var j = 0; j<texts.length; j++){
						ctx.fillText(texts[j], -ctx.measureText(texts[j]).width / 2, j * line_height);
					}
				}else{
					//在画布上绘制填色的文本。文本的默认颜色是黑色
					//measureText()方法返回包含一个对象，该对象包含以像素计的指定字体宽度
					ctx.fillText(text, -ctx.measureText(text).width / 2, 0);
				}
				//绘制图片
				if(award.img){
					ctx.drawImage(award.img,-15,10);
				}
				//把当前画布返回（调整）到上一个save()状态之前
				ctx.restore();
				//----绘制奖品结束----
			}
		}
	}
	
	/**
	 * @param {Number} n	开始
	 * @param {Number} m	结束
	 * @param {Boolean} remove_itself 是否去除自己
	 * 获取n到m之间的随机数
	 */
	function random(n, m, remove_itself){
		if(remove_itself){
			n++;
			m--;
		}
		var random = Math.floor(Math.random()*(m-n+1)+n);
		return random;
	}
	
	/**
	 * @param {Number} angles	奖品位置
	 * @param {String} text	提示语,当初错失提示
	 * 旋转转盘
	 */
	function rotateFn(angles, text){
		if(angles<270){
			angles = 270 - angles; 
		}else{
			angles = 360 - angles + 270;
		}
		$('#mtrWheelCanvas').stopRotate();
		$('#mtrWheelCanvas').rotate({
			angle:0,
			animateTo:angles+1800,
			duration:8000,
			callback:function (){
				if(text){
					alert(text);
				}else{
					onwinning(winning);
				}
				bRotate = !bRotate;
			}
		});
	}
	
	/**
	 * 开始旋转转盘
	 */
	function start(){
		if(bRotate) return;
		bRotate = !bRotate;
		//获取随机数(奖品个数范围内)
		var item = random(0, awards.length-1);
		
		//增加谢谢参与可能性
		var r = random(0, heiheihei);
		if(r >= awards.length){
			item = 0;
		}
		console.log(r);
		
		winning = awards[item];
		var angles = random(winning.startAngle + 5, winning.endAngle - 5, true);
		rotateFn(angles);
	}
	
	/**
	 * @param {Object} award 中奖的信息
	 * 当中将时执行的操作
	 */
	var onwinning = function (award){
		alert(award.name);
		console.log(award);
	};
	
	/**
	 * 初始化操作
	 */
	function init(){
		var $lt = $('<div class="mtr-banner"><div class="mtr-turnplate" >'
			+ '<canvas class="mtr-item" id="mtrWheelCanvas" width="422px" height="422px"></canvas>'
			+ '<img class="mtr-pointer" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIUAAACzCAMAAACQJy++AAABL1BMVEUAAADmLS3kKCfkKSjmLS3iJybmLS3mLS3nLCviKSjjKCjmLS3mLS3mLS3mLS3mLS3mLS3mLS3mLCzmLS3nKyvmLS3mLS3kKCfnKyvnKyvnKijmLS3mLS3mLS3mLS3nKSjmJyTmLS3nKyrnKyrnKyrPFhjmLS3mLS3nJyTmLS3nKyrnKyrmLS3KFRjmLS3kJyXnIRvOFhjZHh7TGBnKFRjnIRvnIRvKFRjKFRjnIRvMFRjKFRjKFRjKFRjnIRvKFRjnIRvKFRjKFRjmKCXnIRvKFRjmLS3/vgTnIRvynQnkdw/ZVhPNFxriKCjUGx3XICHaIiPPGBrVHR/gbRH8tgXOKhfSOhb4rgf1pgjRGhzfJifVSRTriwzdJSXulAvdYhLnKCbngQ7dHBrhHhrZGxm7h8FwAAAARXRSTlMA8hAE/An4yzwyI9yc6+a0lYnvv3onHhpYUSzi1cSFgHBiTUcW+LuppHRrZl3tjjfrx0D3982+uaeJgHJcTfXZsJuR8JKyswKEAAAI/UlEQVR42uzYaVMaQRAG4BZYNBgNMZ4k8U5ijJZH1MTcV88M7CywxCNojOb4/78hAxY17LC4y+5KW6k8H9Aq+PDWdM/yFvDfP2bKghtgPgU3wNaNSPF8Cugtyw2gN+2+Anpf3C2gt+si/XqmRB3fALUZleIlUNsXZVwnf3ruqhT4FmilRCPFR6C12EyxngZS+80UuA2kdi9TfAJKlhDiDyK+B0pLzRTKLSC030pB+o2210qxBXQsofzChhyQ2dYpXgOZOZ3iBZDZ0ynoSkb6nU5BVzKGhE5BN5I50fAbm3aoSsZeWwqykpF+156CqvdtiLYUZCVjXFzCBrKS8cFIQVIy0ptGinUgcEsYKUhKxryRgqZkfDBSUPQ+vRZCokJUMgqiZQcVopIxL1oeYQNN71vxSYFT0GebRgqSkRSEkYKkZKz6ptjpc+9bMVOQ9L5NneI5KiQlIye0Omr9/XHpmTDPgqD3Ndfi4hJrcpyTaqXs9vPHJWvp8wXzN5Zfy0EfFMbvZdmVhvNL17seE3MPWBhjK0vXVYattUEW3ujcJCRvanaU9SbzZAOSNZnPsggeD0FyUnezLKI7haTK3eoIiy6TTyVS+QdZPCNr8Q9iNsO6aDwt666UiCilW65Vz5xu6xHzuuT8D8Kp1iT6kOWKb5KxaYhhYcAvQsXFK8jaCet024KIrNusU9XFQNLnRAYjVuPlh8zk6EEEKHccyOgGRDAx3JGhjD1wz5hXdibCBR1hhhr2qG7MJbMAPRoy97IqsXc15rUaL4TjYiTSGMtCT+MwQlQwsrJ3KNMQWs7YiTrG4DqeFV0MfUWHvdOQGIussjYDBQgl7X1OnEmMq+KppcsQRt57N9AU967cS0OwtYC9jL+jdyHQxIAZIvkY24FLMRg4jvhDuR9Uv8Y9i4laoiv6JGAeWaadYKKqTFuEq3guqcRkOW0zscLejzomTIa7J9ZowPVIbkOzk6FW08FenNo2KrZ9ql6PbPsIfZ21FVHoIjXANBc9bN5NCZUi56hwXlSv3zn/GjiTTA78zRrziJrigPNDVM5LpcPuM1kJcRTSPPKiwvlBUfvG+bH689VMcc75D2wocW53vyeZCfDzNLBjcl70Hk+p9UYbPRD9Aa0esBnp+8ZqRk3RGggecf4TTSf6mqSg0wzTyj2mKB2r/xU1o5KtBmJ+3v8w5qHTHeMoQqcw9qKkb0iR/23fTJfThqEofA2UNUCANnvaLJ2kbTrpUyAPtkkIXoDgkMn7v0RlqyAhyZLBLpnp9PuByTLDGeno6Gph0lc0xhkIHJYYV2RRgd++9mN83ELKOb6mTKx+NhUsyulEjPEKU1VkUvFqMoTKKb4hFP9cbPLYS/NRQvyZ+CHkhR0Olli+OkBrfFhovGmZSQw4FXqoPz/DOjdMeGdUYXnmkoWvnlor3JxeVncInicxpjm1KVhYiB9zToXNSVR1SWk9uKrKDtG7c2GaQRyWYdRq3mqiMTVF11XSOB1to4JElRu9WlFeELAMTQXaSQrO8TYqfDx8opTyfHYmJf2kCq4esLREW4j4ogrWvWEY/YJXoTaGAQyFFMEZFw6WVAVpfcy0r1JBQSsOOHNqFyE+/qB5ooq5iZkLKnSJcQ2UuxTmDKa0QwQVAQkJzxJ8obHnHVtxahcA7iD6kCBBxSKOKPJqRYUgwUxS8ShNz2/aAseKep2KEGutqY2NM8FvxNQSeZCWfT3tiswyFzgQklR4of3HwKHHJviA+EhRcN0A5YwdqDvAkdZbrQxL5GwqWkAxhLjIH31slXasgsZWGSjo3VSgd1ThSFWU382dBuvOHat4kqo43XFezKSLgQab4DvgQVr/nrOl1g54lCb4rX5mJ7tWaTe3NIyks9m9dr+Xlp1i7cD+ihTi6ddFX4ByoVoI6FX4dgwpucOoxiC4fl+O/CDtSrvj6y6XqS55T/ZNMMHa1hu3iPMCzUBFVaAcaPdxBrRueTNZPJ9Vwa8kQ93GfAEYuow9dSrEncbAipnEKjzL/sMr/m/N5uspsPR0xpgvl6nzPn74sSy6OKBWcS2fLctDjS3awHKvNAZ1J30QFTavApfJK3DbuBpbfE/c4BtnUcEZw9fYogoshZJ6XURV+KnbYoq7TrNkN4qwRkXfJZEhrIklUTEfxHjrQUaWrSIz3haUToqTETNCVMHlxZJoVAXK+BbvIFS1RyPksyZ21CQkKPGrJagwGd50J0b8UU2xqQ6uuTWN16GkSSiCL8wVE0sXWRXg2VMfHg7ogk9UQVOeHBMpGYrjVN4l6EmmYrpSMVnNI8SBvr1kgX+2WVRnZ6VDEGgoG8N1g+S8sM1EVE3RBpEL3SlmLirGiHIMIgXFsbJGRWARpqSzrEXkY4JigJySyFL4E423SHCX7CmRIwlXUepRb4qcIBZnUxV+VHe4NFos9TyGugWQ0lbfN3BXKqaiCt+dkJiizTKwFdakFSdPDaHk6KJhiJ/TeNKIJm/ii7c4TjymF169KLcWVuLlB6MACdwq+oSWkQNzDXs5RsK1vApCYYzMEMNHze0L+Q0pz/QWcxLmjAyS6Tb+m3ggE1i4wcTxQWgprj7cI84aeTJEDEeQTKGV/wUlWvIy9EDFEX+ZMjdGiKH8CZS00RoP+a6PqTXVfG0KMvIXUSmChmO0zjj37kBGPdU1PqVFs181PQI9xYp41zYTzhCtsQ9pOGyirNdt+cQUrzTq+WUgjoesV0zF6516jkuI48XJdN2W0qxDau6QwDiPhkBGDTbgAgkMZ1kuUxKMKmzEdyTyMttIw1ArQs8PJOE5rU0dqoHSrMHGHJWRhOGjk8KTIySh8Qm2oNpEUp7HjlLC4xDJuCnAVtTPUALD0YMjVTB+QQnsF/P8Mg1VQr/d5DhPs/Hj6BklYlxCBn4aKA8qdchEvYcyU/pchKzcZW2O8xrkwNf9Etqe5iXkxEkbbUn5vgD5cX2+lYb9A8iXanvTful2DiF/6p3WJoPz7gP8HYpXt+kGTKNzAn+TD8d7p5p0qHypwQ74dLl3JjeJ0etcFWB3FGtHH/falUa3W0Klbuvs/Pb+x/UB/Oc//x6/AbCrWB6XOtkfAAAAAElFTkSuQmCC"/>' 
			+ '</div><div class="mtr-imgs" style="display:none;" ></div></div>');
		$lt.prependTo(selected || "body");
		var $imgs = $lt.children(".mtr-imgs");
		awards.forEach(function(item, index){
			var img = new Image();
			img.src = item.imgPath;
			$imgs.append(img);
			item.img = img;
		});
		$lt.find(".mtr-pointer").click(start);
		window.onload=function(){
			//绘制转盘
			drawRouletteWheel();
		};
	}
	
	//初始化
	init();
	
	return {
		onwinning: function(ow){
			if(typeof ow === "function")
				onwinning = ow;
		},
		getWinning: function(){
			return winning;
		},
		start: start
	}
}