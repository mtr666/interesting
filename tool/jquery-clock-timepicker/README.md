# jquery-clock-timepicker jQuery 时钟选择插件

### 地址

Jquery插件网: [http://www.jq22.com/jquery-info14369](http://www.jq22.com/jquery-info14369)

github: [https://github.com/loebi-ch/jquery-clock-timepicker](https://github.com/loebi-ch/jquery-clock-timepicker)

### 使用方法

在页面中引入jquery和jquery-clock-timepicker.min.js文件。

```HTML
<script src=""path/to/js/jquery.min.js"></script>
<script src=""path/to/js/jquery-clock-timepicker.min.js"></script>
```

### HTML结构

可以使用一个`<input>`元素来作为时间选择器的容器。
	
```HTML
<input class="time" type="text" value="14:30"/>
```

### 初始化插件

在页面DOM元素加载完毕之后，可以通过下面的方法来初始化jquery-clock-timepicker时间选择器插件。

```javaScript
$('.your-time-field').clockTimePicker(options);
```

### jquery-clock-timepicker 时间选择器插件的可用配置参数有：

- afternoonHoursInOuterCircle：设置为true时，午后小时数显示在外圆而不是内圆。默认为fals
- colors.buttonTextColor：移动手机中按钮上的文本颜色。默认为：'#0797FF'。
- colors.clockFaceColor：时钟表面的颜色。默认为：'#EEEEEE'。
- colors.clockInnerCircleTextColor：内圆上显示的数字的颜色。默认为：'#888888'。
- colors.clockOuterCircleTextColor：外圆上显示的数字的颜色。默认为：'#000000'。
- colors.hoverCircleColor：鼠标hover时圆的颜色。默认为：'#DDDDDD'。
- colors.popupBackgroundColor：弹出框的背景颜色。默认为：'#FFFFFF'。
- colors.popupHeaderBackgroundColor：在移动手机中弹出框头部的颜色。默认为：'#0797FF'。
- colors.popupHeaderTextColor：在移动手机中弹出框头部文字的颜色。默认为：'#FFFFFF'。
- colors.selectorColor：时间选择器的颜色。默认为：'#0797FF'。
- colors.selectorNumberColor：被选择的数字的颜色。默认为：'#FFFFFF'。
- fonts.buttonFontSize：按钮文字的大小。按钮只会显示在移动手机中。默认为20。
- fonts.clockInnerCircleFontSize：内圆文字的大小。默认为12。
- fonts.clockOuterCircleFontSize：外圆文字的大小。默认为14。
- fonts.fontFamily：数字的字体。默认为：'Arial'。
- i18n.cancelButton：取消选择时间的按钮上的文字。默认为：'Cancel'。
- i18n.okButton：确认选择时间的按钮上的文字。默认为：'OK'。
- modeSwitchSpeed：切换小时和分钟动画的时间。默认为500。
- onChange：时间发生改变时的回调函数。默认为：function(newValue, oldValue) {}。
- onClose：时间选择器弹出层关闭时触发。默认为：function() {}。
- onModeSwitch：在时间选择器切换小时和分钟模式时触发。默认为：function(selectionMode) {}。
- onOpen：时间选择器弹出层打开时触发。默认为：function() {}。
- popupWidthOnDesktop：桌面版本弹出层的宽度。单位像素。默认为200像素。
- vibrate：是否触发移动手机的震动效果。默认为：true

