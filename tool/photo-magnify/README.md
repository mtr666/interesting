# 图片点击放大全屏查看

放大后可拖拽,可鼠标滚轮放大缩小

## 使用方法

1. 引入js和css文件.
2. 使用div或其他标签包住img图片标签,并给div标签id
3. 然后new Preview()对象,并传入你定义的id即可,如下:
```javascript
new Preview({
	imgWrap: 'wrap' // 指定该容器里的图片点击预览
})
```

## 原文地址

[原生js图片查看插件](https://www.jq22.com/jquery-info20831)